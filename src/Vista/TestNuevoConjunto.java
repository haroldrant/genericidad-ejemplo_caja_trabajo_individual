/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {
    
    public static void main(String[] args) {
        Conjunto<Integer> c1=new Conjunto(9);
        Conjunto<Integer> c2=new Conjunto(20);
        Conjunto<Integer> c3=new Conjunto(30);
        Conjunto<Integer> c4=new Conjunto(10);
        
        
        
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.
        try{
            //Se llenan los Conjuntos
            agregarElementos(c1,c2,c3,c4);
            //Ordenamiento por inserción(Grupo C) del conjunto 1        
            c1.ordenar();
            System.out.println("***Este es ordenamiento normal***");
            System.out.println(c1.toString());
            //Ordenamiento por burbuja del conjunto 2
            c2.ordenarBurbuja();
            System.out.println("***Este es ordenamiento por burbuja***");
            System.out.println(c2.toString());
            //Concatenar normal entre el conjunto 1 y conjunto 3
            c1.concatenar(c3);
            System.out.println("***Este es concatenar normal***");
            System.out.println(c1.toString());
            System.out.println("***Impresión del arreglo que fue concatenado");
            System.out.println(c3.toString());
            //Concatenar Restrictivo entre conjunto 2 y 4
            c2.concatenarRestrictivo(c4);
            System.out.println("***Este es concatenar restrictivo***");
            System.out.println(c2.toString());
            System.out.println("***Impresión del arreglo que fue concatenado");
            System.out.println(c4.toString());
            //Mostrar número mayor en el conjunto 1
            System.out.println("***Método para encontrar el número mayor***");
            System.out.println("El número mayor en el conjunto 1 es: "+c1.getMayorElemento()+"\n");
            //Eliminar un dato del conjunto
            c1.remover(70);
            System.out.println("***Este es el método para eliminar un dato del conjunto 1, \ndato eliminado: 70***");
            System.out.println(c1.toString());
            
        }
        catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    private static void agregarElementos(Conjunto<Integer> c1, Conjunto<Integer> c2, Conjunto<Integer> c3, Conjunto<Integer> c4) throws Exception {
        c1.adicionarElemento(15);
        c1.adicionarElemento(20);
        c1.adicionarElemento(5);
        c1.adicionarElemento(9);
        c1.adicionarElemento(10);
        c1.adicionarElemento(8);
        c1.adicionarElemento(18);
        c1.adicionarElemento(55);
        c1.adicionarElemento(70);
        
        c2.adicionarElemento(1);
        c2.adicionarElemento(21);
        c2.adicionarElemento(2);
        c2.adicionarElemento(15);
        c2.adicionarElemento(75);
        c2.adicionarElemento(6);
        c2.adicionarElemento(84);
        c2.adicionarElemento(16);
        c2.adicionarElemento(94);
        c2.adicionarElemento(18);
        c2.adicionarElemento(20);
        c2.adicionarElemento(73);
        c2.adicionarElemento(58);
        c2.adicionarElemento(30);
        c2.adicionarElemento(29);
        c2.adicionarElemento(9);
        c2.adicionarElemento(50);
        c2.adicionarElemento(54);
        c2.adicionarElemento(88);
        c2.adicionarElemento(78);
        
        c3.adicionarElemento(49);
        c3.adicionarElemento(54);
        c3.adicionarElemento(89);
        c3.adicionarElemento(87);
        c3.adicionarElemento(76);
        c3.adicionarElemento(74);
        c3.adicionarElemento(77);
        c3.adicionarElemento(4);
        c3.adicionarElemento(7);
        c3.adicionarElemento(3);
        c3.adicionarElemento(15);
        c3.adicionarElemento(12);
        c3.adicionarElemento(17);
        c3.adicionarElemento(11);
        c3.adicionarElemento(13);
        c3.adicionarElemento(23);
        c3.adicionarElemento(22);
        c3.adicionarElemento(27);
        c3.adicionarElemento(24);
        c3.adicionarElemento(19);
        c3.adicionarElemento(26);
        c3.adicionarElemento(28);
        c3.adicionarElemento(25);
        c3.adicionarElemento(35);
        c3.adicionarElemento(34);
        c3.adicionarElemento(36);
        c3.adicionarElemento(48);
        c3.adicionarElemento(45);
        c3.adicionarElemento(99);
        c3.adicionarElemento(44);
        
        c4.adicionarElemento(15);
        c4.adicionarElemento(20);
        c4.adicionarElemento(5);
        c4.adicionarElemento(9);
        c4.adicionarElemento(10);
        c4.adicionarElemento(8);
        c4.adicionarElemento(18);
        c4.adicionarElemento(55);
        c4.adicionarElemento(70);
        c4.adicionarElemento(26);
    }
}
