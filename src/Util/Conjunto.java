/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i>=this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i]=new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar()
    {
        // :)
        // grupo C --> Inserción
        
        for(int j=1;j<this.getLength();j++){
            int key=(int)this.cajas[j].getObjeto();
            int y=j-1;
            while((y>=0)&&((int)this.cajas[y].getObjeto()>key))
            {
                this.cajas[y+1]=this.cajas[y];
                y--;
            }
            Caja nueva = new Caja(key);
            this.cajas[y+1]=nueva;
        }
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja()
    {
        Caja aux;
        for(int j=0;j<this.getLength()-1&&this.cajas[j]!=null;j++){
            for(int k=0;k<this.getLength()-1&&this.cajas[k+1]!=null;k++){
                Comparable comparador = (Comparable) this.cajas[k+1].getObjeto();
                if(comparador.compareTo(this.cajas[k].getObjeto())<0){
                    aux=this.cajas[k+1];
                    this.cajas[k+1]=this.cajas[k];
                    this.cajas[k]=aux;                    
                }
            }
        }
    
    }
    
    /**
     * Elimina un elemento del conjunto y compacta
     * @param objBorrado es el objeto que deseo eliminar
     */
    
    public void remover(T objBorrado) throws Exception
    {
        int j=indexOf(objBorrado);
        if(j==-1)
            throw new Exception("El elemento a eliminar no existe");
        for(;j<this.cajas.length-1;j++)
            this.cajas[j]=this.cajas[j+1];
        this.i--;
        this.cajas[j]=null;
    }
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo)
    {
        Caja<T> []concatenarArreglo = new Caja[this.getCapacidad()+nuevo.getCapacidad()];
        int y=0;
        for(;y<this.cajas.length;y++){
            concatenarArreglo[y]=this.cajas[y];
        }
        
        for(int k=0;k<nuevo.getCapacidad();k++) {
            concatenarArreglo[y] = nuevo.cajas[k];
            y++;
        }
        this.cajas=concatenarArreglo;
        this.i=y;
        nuevo.removeAll();
    }
    
    public void ordenarBurbujaDescendente()
    {   
        Caja aux;
        for(int j=0;j<this.getLength()-1;j++){
            for(int k=0;k<this.getLength()-1;k++){
                if((int)this.cajas[k+1].getObjeto()>(int)this.cajas[k].getObjeto()){
                    aux=this.cajas[k+1];
                    this.cajas[k+1]=this.cajas[k];
                    this.cajas[k]=aux;                    
                }
            }
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        Conjunto<T> conjunto = (Conjunto)obj;
        for(int i=0;i<this.getCapacidad();i++){
            if(this.get(i)!=conjunto.get(i))
                return false;
        }
        return true;
    }
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío.En este proceso SI toma en cuenta los datos repetidos
 Ejemplo:
  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
 conjunto1.concatenar(conjunto2)
  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     * @throws java.lang.Exception
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo) throws Exception
    {
        T borrar;
        for(int j=0;j<this.getCapacidad();j++){            
            for(int k=0;k<nuevo.getCapacidad();k++){
                if(this.cajas[j].getObjeto().equals(nuevo.cajas[k].getObjeto())){
                    borrar = nuevo.cajas[k].getObjeto();
                    nuevo.remover(borrar);
                }
            }
        }        
        this.concatenar(nuevo);
    }
    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
        
        for(int j=0;j<this.getCapacidad();j++)
            msg+=this.cajas[j].getObjeto().toString()+"\n";
        
        
        return msg;
    }    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemento mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int y=1;y<this.getCapacidad();y++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[y].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }
}